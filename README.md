# Hyperbee-Array

Array built on top of [hyperbee](https://github.com/hypercore-protocol/hyperbee).

It tries to follow the interface of the built-in Array type where it makes sense.

Note: modify hyperbee-arrays only through this class (not directly from the hyperbee),
and do not modify the same hyperbee from multiple instances of this class simultaneously.
Doing so can lead to an inconsistent data structure, or unexpected behaviour.

Warning: alfa status

## Install

`npm i hyperbee-array`

## Usage

See [example](./example.js)

## API

`const beeArray = new BeeArray(hyperbee, { subEncoder })`

Make a new hyperbee-array instance from a Hyperbee instance.

You can pass in a [subEncoder](https://www.npmjs.com/package/sub-encoder/) if you wish the array to live in a sub instead of in the main bee.

`await beeArray.ready()`

Ensure the beeArray is ready.

`await beeArray.push(element)`

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push

Note: currently only allows pushing a single element.

`await beeArray.at(index)`

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/at

`const stream = beeArray.createReadStream(options)`

Returns a stream over (a range of) all entries

Accepts the same options as the equivalent method in [hyperbee](https://github.com/hypercore-protocol/hyperbee#stream--dbcreatereadstreamoptions)

The most relevant are 'gt', 'lt', 'gte' and 'lte', which should be integer and indicate the index where to start and end.
