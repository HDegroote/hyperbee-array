const Stream = require('streamx')
const SubEncoder = require('sub-encoder')
const cenc = require('compact-encoding')
const ReadyResource = require('ready-resource')

const SUB_NAME = 'array'

class BeeArray extends ReadyResource {
  #nrAdded
  #initLength

  constructor (bee, { subEncoder, valueEncoding } = {}) {
    super()

    // Splitting the length out in these 2 separate states helps
    // ensure the correct insertion order when calling
    // push multiple times before the array is ready
    this.#nrAdded = 0
    this.#initLength = null

    this.valueEncoding = valueEncoding

    subEncoder ??= new SubEncoder()
    this.keyEncoding = subEncoder.sub(SUB_NAME, cenc.lexint)
    this.bee = bee
  }

  async _open () {
    await this.bee.ready()
    const lastI = (await this.bee.peek({ reverse: true, keyEncoding: this.keyEncoding }))?.key
    this.#initLength = lastI != null ? lastI + 1 : 0
  }

  get length () {
    return this.#initLength + this.#nrAdded
  }

  async at (index) {
    if (!this.opened) await this.ready()

    index = index >= 0 ? index : this.length + index
    if (index < 0) return undefined // too negative

    const entry = await this.bee.get(index, {
      keyEncoding: this.keyEncoding,
      valueEncoding: this.valueEncoding
    })

    return entry?.value
  }

  async push (value) {
    const keyIncrement = this.#nrAdded++ // Must happen sync to guarantee insertion order

    if (!this.opened) await this.ready()

    const key = this.#initLength + keyIncrement
    const newLength = key + 1

    await this.bee.put(key, value, {
      keyEncoding: this.keyEncoding,
      valueEncoding: this.valueEncoding
    })
    return newLength
  }

  createReadStream (opts) {
    const beeStream = this.bee.createReadStream(opts, {
      keyEncoding: this.keyEncoding,
      valueEncoding: this.valueEncoding
    })

    const valueExtractor = new Stream.Transform(
      { transform: extractValue }
    )

    beeStream.pipe(valueExtractor)
    return valueExtractor
  }
}

function extractValue (data, callback) {
  callback(null, data.value)
}

module.exports = BeeArray
