const Hyperbee = require('hyperbee')
const ram = require('random-access-memory')
const Hypercore = require('hypercore')
const BeeArray = require('.')

async function main () {
  const core = new Hypercore(ram)
  const bee = new Hyperbee(core, { valueEncoding: 'json' }) // valueEncoding can be anything allowed by hyperbee

  const bArray = new BeeArray(bee)
  await bArray.push('Entry 0')
  await bArray.push('Entry 1')
  await bArray.push('Entry 2')

  console.log('At 0:', await bArray.at(0)) // 'Entry0
  console.log('At 1:', await bArray.at(1)) // 'Entry1
  console.log('At 10:', await bArray.at(10)) // undefined

  const readStream = bArray.createReadStream({ gt: 0, lt: 2 })
  readStream.on('data', function (d) { console.log('data:', d) })
  // 'Entry1'
}

main()
