const test = require('brittle')
const Hyperbee = require('hyperbee')
const ram = require('random-access-memory')
const Hypercore = require('hypercore')
const toArray = require('stream-to-array')
const Corestore = require('corestore')
const BeeArray = require('.')
const SubEncoder = require('sub-encoder')

async function setup (t, opts) {
  const store = new Corestore(ram.reusable())
  const core = store.get({ name: 'core' })

  const bee = new Hyperbee(core)
  const array = new BeeArray(bee, opts)

  return { bee, array, core, store }
}

test('Correctly inits empty bee', async t => {
  const core = new Hypercore(ram)

  const bee = new Hyperbee(core)
  const array = new BeeArray(bee)

  await array.ready()
  t.is(array.length, 0)
})

test('array.at returns undefined if i out of bounds', async t => {
  const { array } = await setup(t)

  t.is(await array.at(3), undefined)
  t.is(await array.at(-2), undefined)
})

test('array.push and array.at work', async t => {
  const { array } = await setup(t, { valueEncoding: 'utf-8' })

  t.is(await array.at(0), undefined)
  const l1 = await array.push('I am added')
  t.is(l1, 1, 'correct updated length')
  t.is(await array.at(0), 'I am added')

  t.is(await array.at(1), undefined)
  await array.push('I am added too')
  t.is(await array.at(0), 'I am added')
  t.is(await array.at(1), 'I am added too')
  t.is(await array.at(2), undefined)
})

test('Respects order in async pushes', async t => {
  const { array } = await setup(t, { valueEncoding: 'utf-8' })
  t.plan(10 + 2)

  const nr = 10
  const proms = []
  for (let i = 0; i < nr; i++) proms.push(array.push(`My entry${i}`))
  await Promise.all(proms)

  t.is(array.length, nr)
  for (let i = 0; i < nr; i++) t.is(await array.at(i), `My entry${i}`)
  t.is(await array.at(nr + 1), undefined, 'sanity check')
})

test('Basic createReadStream works', async t => {
  const { array } = await setup(t, { valueEncoding: 'utf-8' })

  await array.push('entry1')
  await array.push('entry2')

  const res = await toArray(array.createReadStream())
  t.alike(res, ['entry1', 'entry2'])
})

test('CreateReadStream works with range args', async t => {
  const { array } = await setup(t, { valueEncoding: 'utf-8' })

  await array.push('entry0')
  await array.push('entry1')
  await array.push('entry2')
  await array.push('entry3')

  {
    const res = await toArray(array.createReadStream({ lt: 3, gt: 0 }))
    t.alike(res, ['entry1', 'entry2'], 'correct gt and lt options')
  }

  {
    const res = await toArray(array.createReadStream({ lte: 2, gte: 1 }))
    t.alike(res, ['entry1', 'entry2'], 'correct gte and lte options')
  }

  {
    const res = await toArray(array.createReadStream({ lte: 10, gte: 3 }))
    t.alike(res, ['entry3'], 'okay to specify higher index than present')
  }
})

test('Can get with negative index', async t => {
  const { array } = await setup(t, { valueEncoding: 'utf-8' })

  await array.push('entry0')
  await array.push('entry1')
  await array.push('entry2')

  t.is(await array.at(-1), 'entry2')
  t.is(await array.at(-3), 'entry0')
  t.is(await array.at(-4), undefined)
})

test('Can load an existing hyperbee-array', async t => {
  const { array, bee, core, store } = await setup(t, { valueEncoding: 'utf-8' })

  await array.push('entry0')
  await array.push('entry1')
  await array.push('entry2')

  await bee.close()
  t.ok(core.closed, 'sanity check')

  const newBee = new Hyperbee(store.get({ name: 'core' })) // re-open
  const reopenedArray = new BeeArray(newBee, { valueEncoding: 'utf-8' })
  await reopenedArray.ready()

  t.is(reopenedArray.length, 3)
  await reopenedArray.push('entry3')
  t.is(reopenedArray.length, 4, 'Sanity check: can still push')
})

test('Can pass in a sub encoder, and a bee can contain multiple arrays', async t => {
  const store = new Corestore(ram.reusable())
  const core = store.get({ name: 'core' })

  const subEncoder = new SubEncoder()
  const mainSub = subEncoder.sub('main-sub')
  const bee = new Hyperbee(core, { valueEncoding: 'utf-8' })
  const lowLevelArray = new BeeArray(bee, { subEncoder: mainSub })
  const highLevelArray = new BeeArray(bee)

  await bee.put('Sub', 'here', { keyEncoding: mainSub })
  await bee.put('just in the bee', 'here')

  await lowLevelArray.push('low 1')
  await lowLevelArray.push('low 2')
  await highLevelArray.push('high 1')
  await highLevelArray.push('high 2')

  const lowRes = await toArray(lowLevelArray.createReadStream())
  const highRes = await toArray(highLevelArray.createReadStream())
  const allRes = await toArray(bee.createReadStream())

  t.alike(lowRes, ['low 1', 'low 2'])
  t.alike(highRes, ['high 1', 'high 2'])
  t.is(allRes.length, 6)
})
